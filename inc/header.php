<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Universo EAD</title>
    <link rel="stylesheet" href="assets/style.css">

</head>
<body>
    <div class="header">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h2><a class="logo" href="http://localhost/universoead/"><img src="assets/img/logo.png" alt="Logo"></a></h2>
                </div>
                <div class="col-md-6">		
                    <nav id="nav">
			            <button aria-label="Abrir Menu" id="btn-mobile" aria-haspopup="true" aria-controls="menu" aria-expanded="false">Menu<span id="hamburger"></span></button>
                    <ul id="menu" role="menu">
                        <li><a href="/">Sobre</a></li>
                        <li><a href="/">Produtos</a></li>
                        <li><a href="/">Portfólio</a></li>
                        <li><a href="/">Contato</a></li>
                    </ul>
			    </nav>
            </div>
            </div>
        </div>
    </div>
    <style>

nav#nav {
    float: right;
	margin-top: 16px;
}
#header {
  display: flex;
  align-items: center;
  justify-content: space-between;
}

#menu {
  display: flex;
  list-style: none;
  gap: 0.5rem;
}

#menu a {
  display: block;
  padding: 0.5rem;
}

#btn-mobile {
  display: none;
}

@media (max-width: 992px) {
	ul#menu li {
		display: block;
	}
	ul#menu {
		padding: 0;
	}
  #menu {
    display: block;
    position: absolute;
    width: 100%;
    top: 110px;
    right: 0px;
    background: #ffdd00;
    transition: 0.6s;
    z-index: 1000;
    height: 0px;
    visibility: hidden;
    overflow-y: hidden;
  }
  #nav.active #menu {
    height: calc(100vh - 70px);
    visibility: visible;
    overflow-y: auto;
  }
  #menu a {
    padding: 1rem 0;
    margin: 0 1rem;
    border-bottom: 2px solid rgba(0, 0, 0, 0.05);
  }
  #btn-mobile {
    display: flex;
	padding: 10px 0 0 0;
    font-size: 1rem;
    border: none;
    background: none;
    cursor: pointer;
    gap: 0.5rem;
  }
  #hamburger {
    border-top: 2px solid;
    width: 20px;
  }
  #hamburger {
	position: relative;
    top: 5px;
}
  #hamburger::after,
  #hamburger::before {
    content: '';
    display: block;
    width: 20px;
    height: 2px;
    background: currentColor;
    margin-top: 5px;
    transition: 0.3s;
    position: relative;
  }
  #nav.active #hamburger {
    border-top-color: transparent;
  }
  #nav.active #hamburger::before {
    transform: rotate(135deg);
  }
  #nav.active #hamburger::after {
    transform: rotate(-135deg);
    top: -7px;
  }
}
</style>





    <script>
	const btnMobile = document.getElementById('btn-mobile');
	function toggleMenu(event) {
	if (event.type === 'touchstart') event.preventDefault();
	const nav = document.getElementById('nav');
	nav.classList.toggle('active');
	const active = nav.classList.contains('active');
	event.currentTarget.setAttribute('aria-expanded', active);
	if (active) {
		event.currentTarget.setAttribute('aria-label', 'Fechar Menu');
	} else {
		event.currentTarget.setAttribute('aria-label', 'Abrir Menu');
	}
	}
	btnMobile.addEventListener('click', toggleMenu);
	btnMobile.addEventListener('touchstart', toggleMenu);
</script>