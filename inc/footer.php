<!doctype html>
<html lang="en" class="h-100">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="">
      <meta name="author" content="">
      <meta name="generator" content="">
      <title>Footer</title>
      <link href="https://getbootstrap.com/docs/5.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
      <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet'>
      <style type="text/css">
         body{
         font-family: 'Roboto';
         }
         footer .text-menu{
          color: #30DABD;
         }
         footer .accordion{
         background-color: rgba(0, 0, 0, 0);
         --bs-accordion-bg: rgb(0 0 0 / 8%);
         --bs-accordion-border-color: none;
         --bs-accordion-bg: transparent;
         --bs-accordion-border-radius: 0px;
         --bs-accordion-inner-border-radius: 0;
         /*--bs-accordion-border-bottom-color: pink;*/
         }
         footer .accordion-button {
          border: 2px solid transparent;
          border-bottom-color: white;
          background-color: rgba(0, 0, 0, 0)!important;
         }
         footer .accordion-button:not(.collapsed) {
         background-color: rgba(0, 0, 0, 0);
         --bs-accordion-bg: rgb(0 0 0 / 0%);
         }
         footer .accordion-button::after {
         filter: invert(68%) sepia(46%) saturate(616%) hue-rotate(119deg) brightness(97%) contrast(90%);
         }
         footer .accordion-button:focus{
          /*border-color: none!important;*/
         }
         footer .accordion-button.hidden::after {
          display: none;
         }

         @media (min-width: 992px) {
          footer .accordion-collapse{
            display: block!important;
          }
         }

         ::-webkit-scrollbar {
            width: 10px;
          }

          /* Track */
          ::-webkit-scrollbar-track {
            box-shadow: inset 0 0 5px grey; 
            border-radius: 10px;
          }
           
          /* Handle */
          ::-webkit-scrollbar-thumb {
            background: #30DABD;
            border-radius: 10px;
          }

          /* Handle on hover */
          ::-webkit-scrollbar-thumb:hover {
            background: #187061; 
          }
          footer img{
            user-drag: none;
            -webkit-user-drag: none;
            user-select: none;
            -moz-user-select: none;
            -webkit-user-select: none;
            -ms-user-select: none;
          }
      </style>
   </head>
   <body class="d-flex flex-column h-100">
      <!-- Begin page content -->
      <!-- <main class="flex-shrink-0">
         <div class="container">
            <h1 class="mt-5">Teste</h1>
            <p class="lead">teste</p>
            <p>teste</p>
         </div>
      </main> -->
      <footer class="mt-auto">
         <div style="background: linear-gradient(270deg, #00538A 12.09%, #3BB6C6 145.18%);">
            <div class="container pt-5 pb-3" >
               <div class="row pb-5">
                  <a href="./" class="mx-auto text-center" style="display: contents;">
                    <img src="assets/img/unicesumar.svg" alt="Unicesumar" width="206" height="40" class="d-block mx-auto">
                  </a>
               </div>
               <div class="row">
                  <div class="col-12 col-lg-2 mb-3">
                     <div class="accordion">
                        <div class="accordion-item">
                           <h2 class="accordion-header" id="headingOne">
                              <button class="accordion-button collapsed text-white" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                              Pessoas
                              </button>
                           </h2>
                           <div id="collapseOne" class="accordion-collapse collapse" aria-labelledby="headingOne">
                              <div class="accordion-body">
                                 <ul class="nav flex-column">
                                    <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-menu">Todos os cursos</a></li>
                                    <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-menu">Pós-Graduação</a></li>
                                    <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-menu">Profissionalizantes</a></li>
                                    <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-menu">Técnicos</a></li>
                                    <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-menu">Livres</a></li>
                                    <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-menu">Vgas / Plug</a></li>
                                    <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-menu">Assinaturas</a></li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-12 col-lg-2 mb-3">
                     <div class="accordion">
                        <div class="accordion-item">
                           <h2 class="accordion-header" id="headingTwo">
                              <button class="accordion-button collapsed text-white"  type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                              Empresas
                              </button>
                           </h2>
                           <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo">
                              <div class="accordion-body">
                                 <ul class="nav flex-column">
                                    <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-menu">Produtos</a></li>
                                    <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-menu">Treinamentos</a></li>
                                    <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-menu">Soluções</a></li>
                                    <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-menu">RH / Plug</a></li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-12 col-lg-2 mb-3">
                     <div class="accordion">
                        <div class="accordion-item">
                           <h2 class="accordion-header" id="headingThree">
                              <button class="accordion-button collapsed text-white"  type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                              Governo
                              </button>
                           </h2>
                           <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree">
                              <div class="accordion-body">
                                 <ul class="nav flex-column">
                                    <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-menu">Militar</a></li>
                                    <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-menu">Infraestrutura</a></li>
                                    <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-menu">Educação</a></li>
                                    <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-menu">Energia</a></li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-12 col-lg-2 mb-3">
                     <div class="accordion">
                        <div class="accordion-item">
                           <h2 class="accordion-header" id="headingFour">
                              <button class="accordion-button collapsed text-white"  type="button" data-bs-toggle="collapse" data-bs-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                              Atendimento
                              </button>
                           </h2>
                           <div id="collapseFour" class="accordion-collapse collapse" aria-labelledby="headingFour">
                              <div class="accordion-body">
                                 <ul class="nav flex-column">
                                    <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-menu">0800 600 6360</a></li>
                                    <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-menu">Features</a></li>
                                    <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-menu">Pricing</a></li>
                                    <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-menu">FAQs</a></li>
                                    <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-menu">About</a></li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-3 offset-lg-1 mb-3">
                     <form>
                        <h5 class="text-center text-white py-4 pt-lg-0">Pagamentos</h5>
                        <div class="d-flex flex-column flex-sm-row w-100 justify-content-center">
                           <ul class="list-unstyled d-flex flex-wrap justify-content-center gap-1">
                              <li>
                                 <img src="assets/img/visa.svg" alt="Visa" class="user-select-none" unselectable="on">
                              </li>
                              <li>
                                 <img src="assets/img/master-card.svg" alt="Master Card">
                              </li>
                              <li>
                                 <img src="assets/img/american-express.svg" alt="American Express">
                              </li>
                              <li>
                                 <img src="assets/img/diners-club.svg" alt="Diners Club International">
                              </li>
                              <li>
                                 <img src="assets/img/hipercard.svg" alt="Hipercard">
                              </li>
                           </ul>
                        </div>
                     </form>
                  </div>
               </div>
               <div class="container">
                  <div class="border-bottom py-1"></div>
               </div>
               <div class="d-flex flex-column align-items-center justify-content-between">
                  <h5 class="text-center text-white py-4">Redes Sociais</h5>
                  <ul class="list-unstyled d-flex flex-wrap gap-3">
                     <li class="mx-auto">
                        <a class="link-dark" href="https://www.facebook.com/eadunicesumaroficial/" target="_blank">
                           <img src="assets/img/facebook.svg" alt="Facebook">
                        </a>
                     </li>
                     <li class="mx-auto">
                        <a class="link-dark" href="https://www.instagram.com/eadunicesumaroficial/" target="_blank">
                           <img src="assets/img/instagram.svg" alt="Instagram">
                        </a>
                     </li>
                     <li class="mx-auto">
                        <a class="link-dark" href="https://twitter.com/eadunicesumar" target="_blank">
                           <img src="assets/img/twitter.svg" alt="Twitter">
                        </a>
                     </li>
                     <li class="mx-auto">
                        <a class="link-dark" href="https://www.youtube.com/channel/UC_hOtRM1VXOdfRwD0ooAIEA" target="_blank">
                           <img src="assets/img/youtube.svg" alt="Youtube">
                        </a>
                     </li>
                     <li class="mx-auto">
                        <a class="link-dark" href="https://www.linkedin.com/school/unicesumaroficial/mycompany/" target="_blank">
                           <img src="assets/img/linkedin.svg" alt="Linkedin">
                        </a>
                     </li>
                  </ul>
               </div>
            </div>
         </div>
         <div class="d-flex flex-column-reverse align-items-center justify-content-between py-4 text-white" style="background: #00538A;">
            <!-- <p>&copy; 2022 Company, Inc. All rights reserved.</p> -->
            <div class="container">
               <p class="text-center">Unicesumar</p>
               <p class="text-center">CNPJ: 79.265.617/0001-99, Av. Guedner, 1610 - Jardim Aclimação, Maringá - PR, 87050-900  - Todos os direitos reservados</p>
            </div>
         </div>
      </footer>
      <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
      <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.5/dist/umd/popper.min.js" integrity="sha384-Xe+8cL9oJa6tN/veChSP7q+mnSPaj5Bcu9mPX5F5xIGE0DVittaqT5lorf0EI7Vk" crossorigin="anonymous"></script>
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.min.js" integrity="sha384-ODmDIVzN+pFdexxHEHFBQH3/9/vQ9uori45z4JjnFsRydbmQbmL5t1tQ0culUzyK" crossorigin="anonymous"></script>
      <script type="text/javascript">
        $(document).ready(function() {
          if( $(window).width() >= 992){

            $('.accordion-button').addClass('hidden');
            var toggle = false;
            $("footer").on('show.bs.collapse', function (e) {
                if (!toggle) {
                  e.preventDefault();
                }
            });
          }else{
            $('.accordion-button').removeClass('hidden');
          }

          // $(window).resize(function(){
          //   if( $(window).width() >= 992){
          //     $('.accordion-button').addClass('hidden');
          //     var toggle = false;
          //     $("footer").on('show.bs.collapse', function (e) {
          //         if (!toggle) {
          //           e.preventDefault();
          //         }
          //     });
          //   }else{
          //     $('.accordion-button').removeClass('hidden');
          //     var toggle = true;
          //     $("footer").on('show.bs.collapse', function (e) {
          //         if (toggle) {
          //           console.log(e);
          //         }
          //     });
          //   }
          // });
        });
      </script>
   </body>
</html>